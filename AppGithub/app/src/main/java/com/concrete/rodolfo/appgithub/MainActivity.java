package com.concrete.rodolfo.appgithub;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.concrete.rodolfo.appgithub.GitHubFramework.GitHubService;
import com.concrete.rodolfo.appgithub.GitHubFramework.Model.Repository;
import com.concrete.rodolfo.appgithub.GitHubFramework.Model.RepositoryResponse;

import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl("https://api.github.com/").addConverterFactory(GsonConverterFactory.create()).build();

        GitHubService service = retrofit.create(GitHubService.class);

        Call<RepositoryResponse> callRepositories = service.getRepositories(2);

        callRepositories.enqueue(new Callback<RepositoryResponse>() {
            @Override
            public void onResponse(Call<RepositoryResponse> call, Response<RepositoryResponse> response) {
                Log.d("Github", String.valueOf(response.body().getTotal_count()));
            }

            @Override
            public void onFailure(Call<RepositoryResponse> call, Throwable t) {
                Log.d("Github", "error");
            }
        });
        //callRepositories.request();
        Log.d("teste", "teste");
    }
}
