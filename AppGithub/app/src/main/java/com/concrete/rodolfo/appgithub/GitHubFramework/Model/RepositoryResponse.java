package com.concrete.rodolfo.appgithub.GitHubFramework.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by rodolfo on 31/10/2017.
 */

public class RepositoryResponse implements Serializable {

    @SerializedName("total_count")
    private int total_count;

    @SerializedName("incomplete_results")
    private boolean incomplete_results;

    @SerializedName("items")
    private List<Repository> repositories;

    public int getTotal_count() {
        return total_count;
    }

    public boolean isIncomplete_results() {
        return incomplete_results;
    }

    public List<Repository> getRepositories() {
        return repositories;
    }
}
