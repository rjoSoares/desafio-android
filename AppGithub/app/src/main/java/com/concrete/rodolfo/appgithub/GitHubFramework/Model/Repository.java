package com.concrete.rodolfo.appgithub.GitHubFramework.Model;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by rodolfo on 30/10/2017.
 */

public class Repository implements Serializable{

    @SerializedName("id")
    private int id;

    @SerializedName("name")
    private String name;

    @SerializedName("full_name")
    private String full_name;

    @SerializedName("owner")
    private User owner;

    @SerializedName("forks_counts")
    private int forks_counts;

    @SerializedName("description")
    private String description;

    @SerializedName("startgazers_count")
    private int stargazers_count;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getFull_name() {
        return full_name;
    }

    public User getOwner() {
        return owner;
    }

    public int getForks_counts() {
        return forks_counts;
    }

    public String getDescription() {
        return description;
    }

    public int getStargazers_count() {
        return stargazers_count;
    }
}
