package com.concrete.rodolfo.appgithub.GitHubFramework.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rodolfo on 30/10/2017.
 */

public class User {

    @SerializedName("id")
    private int id;

    @SerializedName("login")
    private String login;

    @SerializedName("avatar_url")
    private String avatar;

}
