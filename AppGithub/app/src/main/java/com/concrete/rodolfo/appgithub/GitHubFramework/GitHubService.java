package com.concrete.rodolfo.appgithub.GitHubFramework;

import com.concrete.rodolfo.appgithub.GitHubFramework.Model.PullRequest;
import com.concrete.rodolfo.appgithub.GitHubFramework.Model.Repository;
import com.concrete.rodolfo.appgithub.GitHubFramework.Model.RepositoryResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by rodolfo on 30/10/2017.
 */

public interface GitHubService {

    @GET("search/repositories?q=language:Java&sort=stars")
    Call<RepositoryResponse> getRepositories(@Query("page") int page);

    @GET("repos/{user}/{repository}/pulls")
    Call<List<PullRequest>> getPullRequests(@Path("user") String user, @Path("repository") String repository);
}
