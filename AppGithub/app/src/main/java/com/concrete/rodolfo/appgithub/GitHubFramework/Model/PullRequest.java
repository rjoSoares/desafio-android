package com.concrete.rodolfo.appgithub.GitHubFramework.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by rodolfo on 30/10/2017.
 */

public class PullRequest implements Serializable{

    private String name;
    private String description;

    @SerializedName("user")
    private User owner;

    @SerializedName("html_url")
    private String url;

    @SerializedName("created_at")
    private Date createdAt;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public User getOwner() {
        return owner;
    }

    public String getUrl() {
        return url;
    }

    public Date getCreatedAt() {
        return createdAt;
    }
}
